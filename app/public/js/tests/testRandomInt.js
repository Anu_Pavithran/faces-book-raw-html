'use strict';

module.exports = testRandomInt;

const randomInt = require('./../randomInt.js');
const assert = require('assert');


function testRandomInt() {
  test_randomInt_with_lower_bound_included();
  test_randomInt_with_upper_bound_excluded();
  test_random_int_covers_all_numbers();
}

function test_randomInt_with_lower_bound_included() {
  let count = [0,0,0,0];
  for (let i = 0; i < 1000; i ++){
  count[randomInt(1,5)-1]++;
  }
    assert.ok(count[0] !==0);
    assert.ok(1 <= randomInt(1,5));
}

function test_randomInt_with_upper_bound_excluded() {
   assert.ok(5 > randomInt(1,5));
}

function test_random_int_covers_all_numbers(){
  let count = [0,0,0,0];
  for (let i = 0; i < 1000; i ++){
  count[randomInt(1,5)-1]++;
  }
  for( let i = 0;i<4;i++){
    assert.ok(count[i] !== 0);
  }
/*  for( let i = 0;i<4;i++){
    console.log(count[i]);
  }*/
}
testRandomInt();

//console.log('All tests passed');
