'use strict';

module.exports = test_shuffle;

const shuffle = require('./../shuffle.js');
const assert = require('assert');


function test_shuffling() {
  let y = [1,2,3,4,5,6,7];
  let x = y.slice(0);
  assert.ok(arrayEqual(x,y));
  shuffle(y);
  assert.ok(!arrayEqual(x,y));
  assert.ok(arrayEqual(x.sort(), y.sort()));
}

function test_shuffle(){
  test_shuffling();
}

function arrayEqual(x,y) {
  assert.equal(x.length, y.length);
  for (let i = 0; i < x.length; i ++){
     if(x[i] !== y[i]){
       return false;
     }
  }
  return true;
}

test_shuffle();
console.log('All my tests passed');
