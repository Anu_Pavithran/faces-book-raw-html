'use strict';

module.exports = randomInt;

function randomInt(lo,hi) {
  return Math.floor(Math.random() * (hi - lo)) + lo;
}
