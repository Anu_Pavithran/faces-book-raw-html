$(document).ready(function() {

  // ready for Javascript which will run when the html is loaded
  const shuffle = require('./shuffle.js');

  const faces = $('faces');
  //const add = $('<b>').html();
  //faces.append(add)
  const makeFace = function(name){
    return $('<img>', {
      src : `img/2018/${name}.jpg`,
      class : "face",
      title : name
    });
  }

  const names = ["Thiru", "Thamizh","Janani","Sudeep","Ameya","John", "Varsha","Vishnu", "Gayatri", "Supriya","Santhosh",
     "Shruti","Rishi","Sindhu","Pavithrann","Jon", "Aishu","Mariah","Anushree","RVishnuPriya","Mahidher","Sanjana",
    "Akhil","Karthika","Keerthana","Akshat"
  ];
  //shuffle(names);
  names.forEach(function(name) {
    faces.append(makeFace(name));
  });

});
