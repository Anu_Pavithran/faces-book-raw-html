'use strict';

const randomInt = require('./randomInt.js');

module.exports = shuffle;

function shuffle(x) {
  for(let i = 0; i < x.length; i++) {
    let j = randomInt(0, x.length);
    let temp = x[i];
    x[i] = x[j];
    x[j] = temp;
  }
}
